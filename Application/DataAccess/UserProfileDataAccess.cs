﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.Domain;
using Timesheet.Domain.ViewModel;
using Timesheet.Repository;

namespace DataAccess
{
    public class UserProfileDataAccess : IUserProfileDataAccess
    {
        private GenericRepository<ApplicationUser> _userRepository;
        ApplicationDbContext obj = new ApplicationDbContext();
        public UserProfileDataAccess()
        {
            _userRepository = new GenericRepository<ApplicationUser>(new ApplicationDbContext());
        }

        public ApplicationUser GetUser(string id)
        {
           var result = _userRepository.GetByID(id);
           return result;
        }


        public void UpdateUserProfile(ApplicationUser user)
         {
           
           _userRepository.Update(user);
            _userRepository.SaveChange();
        }
    }
}
