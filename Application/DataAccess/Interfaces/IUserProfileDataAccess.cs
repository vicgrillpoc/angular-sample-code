﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.Domain;
using Timesheet.Domain.ViewModel;
namespace DataAccess.Interfaces
{
    public interface IUserProfileDataAccess
    {
        ApplicationUser GetUser(string id);
        void UpdateUserProfile(ApplicationUser user);
    }
}
