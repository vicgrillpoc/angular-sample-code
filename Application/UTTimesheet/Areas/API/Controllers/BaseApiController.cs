﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UITimesheet.Areas.Controllers.API 
{
    [Authorize]
    public class BaseApiController : ApiController
    {
        public BaseApiController()
        {

        }
    }
}
