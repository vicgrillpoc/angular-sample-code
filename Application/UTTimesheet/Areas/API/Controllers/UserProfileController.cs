﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UITimesheet.Areas.Controllers.API;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using BLTimesheet;
using Timesheet.Domain.Models;
using Timesheet.Domain.ViewModel;
using Timesheet.Domain;

namespace UserLogin.Areas.API.Controllers
{
    public class UserProfileController : BaseApiController
    {
        private IUserProfile _userProfile;
         
        public UserProfileController()
        {
            _userProfile = new UserProfile();
        }

        [HttpGet]
        public IHttpActionResult GetUser()
        {
          

            var result = _userProfile.GetUser(User.Identity.GetUserId());
            return Ok(result);
        }

        [HttpPost]
        public IHttpActionResult UpdateUser(UserProfileViewModel user)
        {
            _userProfile.UpdateUserProfile(user, User.Identity.GetUserId());
            return Ok();
        }


    }
}
