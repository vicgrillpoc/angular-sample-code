﻿'use strict';
dashboard.controller('DashboardCtrl', ['$scope', '$state', "identityService", 'dashboradService', 'notifierService', function ($scope, $state, identityService, dashboradService, notifierService) {

    $scope.logout = function () {
      identityService.clearAccessToken();
       // window.localStorage.clear();
        //window.sessionStorage.clear();
        $state.go('login');
       
    };
    //alert(identityService.getAccessToken());

    if (!identityService.getAccessToken()) { $state.go('login'); }
    $scope.init = function () {       
        dashboradService.getUserData(identityService.getSecurityHeaders()).then(function (result) {
         
            $scope.Email = result.data.Email;
            $scope.FirstName = result.data.FirstName;
            $scope.LastName = result.data.LastName;
            $scope.PhoneNumber = result.data.PhoneNumber;

        }, function (error) {
            console.log(error);
            //var responseNotify = {
            //    responseType: "error",
            //    message: error.data.statusText
            //};
            //notifierService.notify(responseNotify);
        });
    };
    $scope.init();

    $scope.viewDetail = true;
    $scope.update = false;

    $scope.Edit = function () {
        $scope.viewDetail = false;
        $scope.update = true;
    };

    $scope.updateData = function () {
        $scope.submitted = true;
        if ($scope.userForm.$valid) {
            $scope.viewDetail = true;
            $scope.update = false;
            var register = {
                FirstName: $scope.FirstName,
                LastName: $scope.LastName,
                PhoneNumber: $scope.PhoneNumber
            }
            dashboradService.updateUser(register, identityService.getSecurityHeaders()).then(function (result) {
                $scope.alertShow = true;
                $scope.alertShowError = false;
                $scope.alert='Your Data Has Been Updated Successfully'
                var responseNotify = {
                    responseType: "success",
                    message: 'Your Data Has Been Updated Successfully'
                };
                notifierService.notify(responseNotify);
            }, function (error) {
                var responseNotify = {
                    responseType: "error",
                    message:error.data.Message
                };
                notifierService.notify(responseNotify);
                $scope.alertShow = false;
                $scope.alertShowError = true;
                $scope.alert = error.data.Message;
            });
        }
    };
    $scope.alertShow = false;
    $scope.alertShowError = false;
    $scope.alertMessage = function () {
        $scope.alertShow = false;
        $scope.alertShowError = false;
    };


}]);