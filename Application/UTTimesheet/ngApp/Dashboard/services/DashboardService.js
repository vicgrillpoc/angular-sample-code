﻿'use strict';
dashboard.factory('dashboradService', ['apiService', '$q', '$http', 'identityService', function (apiService, $q, $http, identityService) {

    var getUserData = function (header) {     
        var deferred = $q.defer();      
        return apiService.getAuth('/api/UserProfile/GetUser', { headers: header }).success(function (result) {
            deferred.resolve(result);
        }).error(function (error) {
            return deferred.promise;
        });
    };

    var updateUser = function (register, header) {
        var deferred = $q.defer();
        return apiService.postOAuth('/api/UserProfile/UpdateUser', register, { headers: header }).success(function (result) {
            deferred.resolve(result);
        }).error(function (error) {
            return deferred.promise;
        });
    };

    return {
        getUserData: getUserData,
        updateUser: updateUser
    };


}]);