﻿'use strict';
function apiService($http) {
    var post = function (url,data) {
        return $http.post(url, data);
    };
    var get = function (url, data) {
        return $http.get(url, data);
    };

    var getAuth = function (url, header) {       
        return $http.get(url, header);
    }
    var postOAuth = function (url, data, header) {
        
        return $http.post(url, data, header);
    };
    return {
        post: post,
        get: get,
        getAuth: getAuth,
        postOAuth: postOAuth
    };
};

account.factory('apiService', ['$http', apiService]);
  