﻿'use strict';
timeSheet.run([
        "$rootScope", "$timeout", "$location", "identityService", "utilityService","$state",
        function ($rootScope, $timeout, $location, identityService, utilityService,$state) {
            var fired = false;
            $rootScope.$on("$locationChangeStart", function (event) {
                var fragment = utilityService.getFragment(),
                    externalAccessToken,
                    externalError,
                    loginUrl;

                if (fragment["/access_token"]) {
                    fragment.access_token = fragment["/access_token"];
                    event.preventDefault();
                }
                else { return; }

                if (fired) return;
                fired = true;
                $timeout(function () { fired = false; }, 1000);

                identityService.restoreSessionStorageFromLocalStorage();
                identityService.verifyStateMatch(fragment);

                if (typeof (fragment.error) !== "undefined") {
                    utilityService.cleanUpLocation();
                    $location.path("/login");

                } else if (typeof (fragment.access_token) !== "undefined") {

                    utilityService.cleanUpLocation();
                    identityService.getUserInfo(fragment.access_token).success(function (data) {

                        if (typeof (data.Email) !== "undefined" && typeof (data.HasRegistered) !== "undefined" && typeof (data.LoginProvider) !== "undefined") {
                            if (data.HasRegistered) {
                                identityService.setAuthorizedUserData(data);
                                identityService.setAccessToken(fragment.access_token, false);
                                $state.go("dashboard");

                            } else if (typeof (sessionStorage["loginUrl"]) !== "undefined") {

                                loginUrl = sessionStorage["loginUrl"];
                                sessionStorage.removeItem("loginUrl");

                                var externalRegister = {
                                    data: data,
                                    fragment: fragment,
                                    loginUrl: loginUrl
                                };

                                sessionStorage.setItem("ExternalRegister", JSON.stringify(externalRegister));
                                $state.go("externalRegister");
                            } else {
                                $state.go("login");
                            }
                        } else {
                            $location.path("/login");
                        }
                    }).error(function () {
                        $location.path("/login");
                    });
                } else {
                    if (sessionStorage["accessToken"] || localStorage["accessToken"]) {
                        identityService.getUserInfo().success(function (result) {
                            if (result.userName) {
                                identityService.setAuthorizedUserData(result);
                            } else {
                                $location.path("/login");
                            }
                        });
                    }
                }
            });
        }
]);