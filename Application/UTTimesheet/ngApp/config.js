﻿'use strict';
timeSheet.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
          url: '/login',
          templateUrl: "/ngApp/Account/views/login.html",
          controller: 'LoginCtrl'
      })
    .state('register', {
        url: '/register',
        templateUrl: "/ngApp/Account/views/register.html",
        controller: 'RegisterCtrl'
    }).state('verify', {
        url: '/verify',
        templateUrl: "/ngApp/Account/views/verifyuser.html",
        controller: 'VerifyCtrl'
    }).state('dashboard', {
        url: '/dashboard',
        templateUrl: "/ngApp/Dashboard/views/dashboard.html",
        controller: 'DashboardCtrl'
    }).state('confirmMail', {
        url: '/confirmMail',
        templateUrl: "/ngApp/Account/views/confimMail.html",
        controller: 'ConfirmMailCtrl'
    }).state('externalRegister', {
        url: '/externalRegister',
        templateUrl: '/ngApp/Account/views/externalLogin.html',
        controller: 'ExternalLoginCtrl'
    });
        
    $urlRouterProvider.otherwise('/login');
    
}]);

