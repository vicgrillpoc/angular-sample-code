﻿'use strict';
account.factory('identityService', ['apiService', '$q', '$rootScope', function (apiService, $q, $rootScope) {

    var registeruser = function (registration) {
        var deferred = $q.defer();
        return apiService.post('api/account/register', registration).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            console.log(error);
        });
        return deferred.promise;
    };

    var verfiy = function (registration) {
        var deferred = $q.defer();
        return apiService.post('api/account/ConfirmEmail', registration).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            console.log(error);
        });
        return deferred.promise;
    };

    var loginUser = function (data) {
        var deferred = $q.defer();
        return apiService.postOAuth('token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            console.log(error);
        });
        return deferred.promise;
    }

    var getExternalLogins = function () {
        var config = {
            params: {
                returnUrl: "/",
                generateState: true
            }
        };
      
        return apiService.get("/api/Account/ExternalLogins", config);
    };



    var archiveSessionStorageToLocalStorage = function () {
        var backup = {};

        for (var i = 0; i < sessionStorage.length; i++) {
            backup[sessionStorage.key(i)] = sessionStorage[sessionStorage.key(i)];
        }

        localStorage["sessionStorageBackup"] = JSON.stringify(backup);
        sessionStorage.clear();
    };

    var restoreSessionStorageFromLocalStorage = function () {
        var backupText = localStorage["sessionStorageBackup"],
            backup;

        if (backupText) {
            backup = JSON.parse(backupText);

            for (var key in backup) {
                sessionStorage[key] = backup[key];
            }

            localStorage.removeItem("sessionStorageBackup");
        }
    };

    var verifyStateMatch = function (fragment) {
        var state;

        if (typeof (fragment.access_token) !== "undefined") {
            state = sessionStorage["state"];
            sessionStorage.removeItem("state");

            if (state === null || fragment.state !== state) {
                fragment.error = "invalid_state";
            }
        }
    };

    var getAuthorizedHeaders = function (accessToken) {
        return { "Authorization": "Bearer " + accessToken };
    };

    var getUserInfo = function (accessToken) {
        var headers;

        if (typeof (accessToken) !== "undefined") {
            headers = getAuthorizedHeaders(accessToken);
        } else {
            headers = getSecurityHeaders();
        }
        var config = {
            headers: headers
        };
        return apiService.get("/api/Account/UserInfo", config);
    };


    var setAuthorizedUserData = function (data) {
        $rootScope.authenticatedUser = data;
    };

    var setAccessToken = function (accessToken, persistent) {
        if (persistent) {
            localStorage["accessToken"] = accessToken;
        } else {
            sessionStorage["accessToken"] = accessToken;
        }
    };

    var clearAccessToken = function () {
        localStorage.removeItem("accessToken");
        sessionStorage.removeItem("accessToken");
        localStorage.removeItem("SendMail");
    };

    var registerExternalUser = function (data, config) {
       
        return apiService.postOAuth("/api/Account/RegisterExternal", data, config);
    };
    var getSecurityHeaders = function () {

        var accessToken = sessionStorage["accessToken"] || localStorage["accessToken"];

        if (accessToken) {
            return getAuthorizedHeaders(accessToken);
        }

        return {};
    };

    var getAccessToken = function () {
        return sessionStorage.getItem("accessToken") || localStorage.getItem("accessToken");
    };


    return {
        register: registeruser,
        verfiyUser: verfiy,
        login: loginUser,
        getExternalLogins: getExternalLogins,
        archiveSessionStorageToLocalStorage: archiveSessionStorageToLocalStorage,
        restoreSessionStorageFromLocalStorage: restoreSessionStorageFromLocalStorage,
        getUserInfo: getUserInfo,
        verifyStateMatch: verifyStateMatch,
        setAuthorizedUserData: setAuthorizedUserData,
        setAccessToken: setAccessToken,
        clearAccessToken: clearAccessToken,
        registerExternalUser: registerExternalUser,
        getAuthorizedHeaders: getAuthorizedHeaders,
        getSecurityHeaders: getSecurityHeaders,
        getAccessToken:getAccessToken

    };
 
}]);