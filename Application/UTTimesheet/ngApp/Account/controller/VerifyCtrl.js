﻿'use strict';
account.controller('VerifyCtrl', ['$rootScope', '$scope', '$state', '$location', 'identityService', 'notifierService', function ($rootScope,$scope, $state, $location, identityService, notifierService) {

    if (!$location.search().code) { $state.go('login'); }
    $scope.registration = {
        NewPassword: "",
        ConfirmPassword: "",
        Code: $location.search().code,
        UserId: $location.search().userId
    };

    $scope.submit = function () {
        $scope.submitted = true;
        if ($scope.userForm.$valid) {
            identityService.verfiyUser($scope.registration).then(function (data) {
                var logData = "grant_type=password&Username=" + data.data.ConfrimedEmail + "&password=" + $scope.registration.NewPassword;
                $rootScope.userMail = data.data.ConfrimedEmail;
                identityService.login(logData).then(function (data) {
                  
                    identityService.setAuthorizedUserData(data.data);
                    identityService.setAccessToken(data.data.access_token, false);
                    $state.go("dashboard");
                    var responseNotify = {
                        responseType: "success",
                        message: 'Welome To Time-Sheet'
                    };
                    notifierService.notify(responseNotify);
                    $state.go('dashboard');
                }, function (error) {
                    console.log(error);
                });

            },
            function (error) {
            });
        }
    };

}]);