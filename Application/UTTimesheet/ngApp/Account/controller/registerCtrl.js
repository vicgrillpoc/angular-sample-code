﻿'use strict';
account.controller('RegisterCtrl', ['$scope', 'identityService', 'notifierService', '$state', function ($scope, identityService, notifierService, $state) {

    $scope.button = true;
    $scope.loader = false;
    $scope.signUp = function () {
        $scope.submitted = true;
        if ($scope.userForm.$valid) {
            $scope.button = false;
            $scope.loader = true;
            identityService.register($scope.registration).then(function (data) {
                $scope.button = true;
                $scope.loader = false;
              
                localStorage.setItem("SendMail", "True");
                var responseNotify = {
                    responseType: "success",
                    message: 'UserRegisterd Plese Confirm You Email'
                };
                notifierService.notify(responseNotify);
                $state.go('confirmMail');
            }, function (error) {
               
                $scope.errors  = _.flatten(_.map(error.data.ModelState, function (items) {
                    return items;
                }));
              
                if (error.data.ExceptionMessage) {
                    $scope.emailError = error.data.ExceptionMessage;
                    $scope.button = true;
                    $scope.loader = false;
                    var responseNotify = {
                        responseType: "error",
                        message: error.data.ExceptionMessage
                    }
                    notifierService.notify(responseNotify);
                }
                else {
                    $scope.emailError = $scope.errors[1];
                    $scope.button = true;
                    $scope.loader = false;
                    var responseNotify = {
                        responseType: "error",
                        message:  error.data.Message
                    }
                    notifierService.notify(responseNotify);
                }
                
            });
        }
    };


}]);