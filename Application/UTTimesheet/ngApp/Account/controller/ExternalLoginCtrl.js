﻿'use strict';
account.controller('ExternalLoginCtrl', ['$scope', '$location', 'identityService', 'notifierService',
    function ($scope, $location, identityService, notifierService) {
    $scope.init = function() {
        $scope.externalRegisterInfo = JSON.parse(sessionStorage.getItem("ExternalRegister"));
        if (!$scope.externalRegisterInfo) {
            $location.path("/login");
        }
    }();

    $scope.registerExternal = function () {
        $scope.externalRegisterFormSubmitted = true;
        if ($scope.ExternalRegisterForm.$valid) {
            if ($scope.externalRegisterInfo) {
                var config = {
                    headers: identityService.getAuthorizedHeaders($scope.externalRegisterInfo.fragment.access_token)
                };
              
                identityService.registerExternalUser({ Email: $scope.register.Email}, config).success(function (data) {

                    sessionStorage["state"] = $scope.externalRegisterInfo.fragment.state;
                    identityService.archiveSessionStorageToLocalStorage();
                    sessionStorage.removeItem("ExternalRegister");
                    window.location = $scope.externalRegisterInfo.loginUrl;

                }).error(function (result) {
                    console.log(result);
                    var responseNotify = {
                        responseType: "error",
                        message: result.Message
                    };
                    notifierService.notify(responseNotify);
                    
                });
            }
        }
    };
}
]);
