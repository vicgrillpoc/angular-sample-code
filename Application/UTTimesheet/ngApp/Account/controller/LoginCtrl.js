﻿'use strict';
timeSheet.controller('LoginCtrl', ['$scope', 'identityService', 'notifierService', '$state', function ($scope, identityService, notifierService, $state) {
    identityService.restoreSessionStorageFromLocalStorage();
    $scope.Submit = function () {
        $scope.submitted = true;
        if ($scope.loginForm.$valid) {
            var data = "grant_type=password&Username=" + $scope.email + "&password=" + $scope.Password;
            identityService.login(data).then(function (data) {
                console.log(data);
                identityService.setAuthorizedUserData(data.data);
                identityService.setAccessToken(data.data.access_token, false);
                $state.go("dashboard");
            }, function (error) {
               // console.log(error);
                $scope.emailError = error.data.error_description;
                var responseNotify = {
                    responseType: "error",
                    message: error.data.error_description//"The user name or password is incorrect"
                };
                notifierService.notify(responseNotify);
            });
        }
    };

    //identityService.getExternalLogins().success(function (result) {     
    //   // $scope.loginProviders = result;
    //}).error( function (error) {
    //    console.log(error);
    //});

    $scope.externalLogin = function (loginProvider) {
        
        sessionStorage["state"] = loginProvider.State;
        sessionStorage["loginUrl"] = loginProvider.Url;
        // IE doesn't reliably persist sessionStorage when navigating to another URL. Move sessionStorage temporarily
        // to localStorage to work around this problem.
        identityService.archiveSessionStorageToLocalStorage();
        window.location = loginProvider.Url;
    };

    $scope.addClass = function (className) {       
        var type = "btn-info"
        switch (className) {
            case "Microsoft":              
                type = "mocrosoft";
                break;
            case "Google":              
                type= "btn-danger";
                break;
            case "Twitter":               
                type = "btn-info";
                break;
            case "Facebook":              
                type = "facebook";
                break;
        }
        return type;
    };

}]);