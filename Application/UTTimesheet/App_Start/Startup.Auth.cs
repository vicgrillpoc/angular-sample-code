﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using UITimesheet.Providers;
using Timesheet.Domain; 
using System.Web;
using Microsoft.Owin.Security.Google;
using Timesheet.Common;  

namespace UITimesheet
{
    public partial class Startup
    {
        static Startup()
        {
            PublicClientId = "self";
            UserManagerFactory = () => HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();



            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId, UserManagerFactory),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };
        }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public static Func<UserManager<ApplicationUser>> UserManagerFactory { get; set; }



        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and role manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);


            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            app.UseMicrosoftAccountAuthentication(
                clientId: OAuthConfiguration.MicrosoftClientId,
                clientSecret: OAuthConfiguration.MicrosoftClientSecret);

            app.UseTwitterAuthentication(
                consumerKey: OAuthConfiguration.TwitterConsumerKey,
                consumerSecret: OAuthConfiguration.TwitterConsumerSecret);

            app.UseFacebookAuthentication(
                appId: OAuthConfiguration.FacebookAppId,
                appSecret: OAuthConfiguration.FacebookAppSecret);

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                ClientId = OAuthConfiguration.GoogleClientId,
                ClientSecret = OAuthConfiguration.GoogleClientSecret,
                Provider = new GoogleOAuth2AuthenticationProvider()
            });
        }
    }
}