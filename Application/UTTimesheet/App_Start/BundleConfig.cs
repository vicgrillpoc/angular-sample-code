﻿using System.Web;
using System.Web.Optimization;

namespace UITimesheet 
{ 
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                       "~/Scripts/angular.js",
                       "~/Scripts/angular-ui-router.js",
                      "~/Scripts/toastr.js",
                      "~/Scripts/Themejs/sb-admin-2.js"));

            bundles.Add(new ScriptBundle("~/bundles/ngApp").Include(               
                      "~/ngApp/app.js",
                      "~/ngApp/appInit.js",
                      "~/ngApp/config.js",
                     
                      "~/ngApp/Account/account.js",
                      "~/ngApp/Services/apiService.js",
                      "~/ngApp/Services/coreUtilService.js",
                      "~/ngApp/Account/services/identityService.js",
                      "~/ngApp/Account/controller/LoginCtrl.js",
                      "~/ngApp/Account/controller/registerCtrl.js",
                       "~/ngApp/Account/controller/VerifyCtrl.js",
                         "~/ngApp/Account/directives/ConfirmPassword.js",
                          "~/ngApp/Account/controller/ExternalLoginCtrl.js",
                          "~/ngApp/Account/controller/ConfimMailCtrl.js",

                         "~/ngApp/Dashboard/dashboard.js",
                          "~/ngApp/Dashboard/services/DashboardService.js",
                         "~/ngApp/Dashboard/controller/dashboardCtrl.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/Theme/sb-admin-2.css",
                      "~/Content/Theme/font-awesome.min.css",
                      "~/Content/toastr.min.css"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
