﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Timesheet.Repository   
{ 
    public interface IReository<TEntity> where TEntity : class
    {
          IQueryable<TEntity> Get();
          TEntity GetByID(object id);
          void Insert(TEntity entity);
          void Delete(object id);
          void Delete(TEntity entityToDelete);
          void Update(TEntity entityToUpdate);
    }
}
