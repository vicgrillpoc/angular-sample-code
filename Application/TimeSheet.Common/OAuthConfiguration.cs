﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace Timesheet.Common  
{
   public static class OAuthConfiguration 
    {
       public static string MicrosoftClientId { get { 
       return ConfigurationManager.AppSettings["MicrosoftClientId"];} }
       public static string MicrosoftClientSecret { get { return ConfigurationManager.AppSettings["MicrosoftClientSecret"]; } }

       public static string TwitterConsumerKey { get { return ConfigurationManager.AppSettings["TwitterConsumerKey"]; } }
       public static string TwitterConsumerSecret { get { return ConfigurationManager.AppSettings["TwitterConsumerSecret"]; } }

       public static string FacebookAppId { get { return ConfigurationManager.AppSettings["FacebookAppId"]; } }
       public static string FacebookAppSecret { get { return ConfigurationManager.AppSettings["FacebookAppSecret"]; } }

       public static string GoogleClientId { get { return ConfigurationManager.AppSettings["GoogleClientId"]; } }
       public static string GoogleClientSecret { get { return ConfigurationManager.AppSettings["GoogleClientSecret"]; } }


    }
}
