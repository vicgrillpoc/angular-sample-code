﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLTimesheet;
using DataAccess.Interfaces;
using DataAccess;
using Timesheet.Domain.ViewModel;
using Timesheet.Domain;

namespace BLTimesheet
{
    public class UserProfile:IUserProfile
    {

        IUserProfileDataAccess userProfileDataAccess;
        public UserProfile()
        {
            userProfileDataAccess = new UserProfileDataAccess();
        }

        public UserProfileViewModel GetUser(string id)
        {
            var user = userProfileDataAccess.GetUser(id);
           
            return new UserProfileViewModel
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber
            };
        }

        public void UpdateUserProfile(UserProfileViewModel user, string id)
        {
            var userData = userProfileDataAccess.GetUser(id);
            userData.FirstName = user.FirstName;
            userData.LastName = user.LastName;
            userData.PhoneNumber = user.PhoneNumber;
            userProfileDataAccess.UpdateUserProfile(userData);
        }


        
    }
}
