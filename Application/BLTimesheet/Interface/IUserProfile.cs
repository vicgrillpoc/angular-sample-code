﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.Domain.Models;
using Timesheet.Domain.ViewModel;

namespace BLTimesheet
{
    public interface IUserProfile 
    {
        UserProfileViewModel GetUser(string id);
        void UpdateUserProfile(UserProfileViewModel user, string id);
    }
}
